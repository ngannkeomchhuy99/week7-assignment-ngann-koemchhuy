<?php
//sum(2,3)
$total= function($arr){
    $sum = 0;
    $size = count($arr);
    for($i= 0 ; $i < $size ; $i++){
      $sum += $arr[$i];  
    }
    return $sum;

};
//sum (2,3)
$value= [2,3];
echo $total($value)."<br>";;
//sum (2,3,4)
$value= [2,3,4];
echo $total($value)."<br>";;
//sum(2,3,4,5)
$value= [2,3,4,5];
echo $total($value)."<br>";

?>