<?php 

interface C { 
public function insideC(); 
} 

interface B { 
public function insideB(); 
} 

class Multiple implements B, C { 

	// Function of the interface B 
	function insideB() { 
		echo "I am in interface B<br>"; 
	} 

	// Function of the interface C 
	function insideC() { 
		echo "I am in interface C<br>"; 
	} 

	public function insidemultiple() 
	{ 
		echo "I am in inherited class<br>"; 
	} 
} 

$obj = new multiple(); 
$obj->insideC(); 
$obj->insideB(); 
$obj->insidemultiple();
