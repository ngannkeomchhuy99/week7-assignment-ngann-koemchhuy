<!-- 
Traits (Using Class along with Traits): The trait is a type of class which enables multiple inheritance.
Classes, case classes, objects, and traits can all extend no more than one class but can extend multiple 
traits at the same time.
 -->

<?php

// Class Geeks 
class A
{
    public function sayhello()
    {
        echo "Hello<br>";
    }
}

// Trait forGeeks 
trait B
{
    public function sayfor()
    {
        echo "bonjour<br>";
    }
}

class Sample extends A
{
    use B;
    public function sayMyname()
    {
        echo "Say my name<br>";
    }
}

$test = new Sample();
$test->sayhello();
$test->sayfor();
$test->sayMyname();
?>