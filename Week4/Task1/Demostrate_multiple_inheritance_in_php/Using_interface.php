<?php 

class A { 
public function insideA() { 
	echo "I am in class A<br >"; 
	} 
} 

interface B { 
public function insideB(); 
} 

class Multiple extends A implements B { 

	function insideB() { 
		echo "\nI am in interface<br>"; 
	} 

	public function insidemultiple() { 
	echo "I am in inherited class<br>"; 
	} 
} 

$obj = new multiple(); 
$obj->insideA(); 
$obj->insideB(); 
$obj->insidemultiple();
