<?php
require_once (__DIR__.'/../Percasement/Percasement.php');
require_once (__DIR__.'/../Percasement/IPaymentMethod.php');
class Wing_payment extends Percasement implements IPaymentMethod {
    public function paymentMethod(){
        return 'Wing';
    }
}
?>