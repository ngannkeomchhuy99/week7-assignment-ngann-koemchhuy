<?php
require_once (__DIR__.'/Payment/ABA_payment.php');
require_once (__DIR__.'/Payment/PiPay_payment.php');
require_once (__DIR__.'/Payment/Wing_payment.php');

 $payments = [
    new ABA_payment('pumpkin',4,1),
    new ABA_payment('pea',3,6),
    new ABA_payment('corn',2,5),
    new ABA_payment('lettuce',1,6),
    new ABA_payment('parsnip',2,3),
    new ABA_payment('ginger',6,1),

    new PiPay_payment('giraff',12,5),
    new PiPay_payment('tigar',1,5),
    new PiPay_payment('kangaroo',5,5),
    new PiPay_payment('raccoon',3,5),


    new Wing_payment('bolt',4,6),
    new Wing_payment('bradawl',4,6),
    new Wing_payment('nut =',4,6),
    
];

echo '<p style="font-size:25px" >1.Number of sales by ABA method</p>';
$NumberSalesByABA = 0;
foreach( $payments as $pay){
    $NumberSalesByABA += $pay->paymentMethod() === 'ABA'? 1:0;
}

echo '<p style="font-size:20px">Number of sales by ABA method: '.$NumberSalesByABA.'</p>';

echo '<p style="font-size:25px" >2.Number of sales by PiPay and Wing method</p>';
$NumberSalesByPiPay = 0;
$NumberSalesByWing = 0;
foreach( $payments as $pay){
    $NumberSalesByPiPay += $pay->paymentMethod() === 'PiPay'? 1:0;
    $NumberSalesByWing += $pay->paymentMethod() === 'Wing'? 1:0;
}

echo '<p style="font-size:20px">Number of sales by PiPay and Wing method: '.$NumberSalesByPiPay + $NumberSalesByWing.'</p>';


function displayTableResult($payments){
    $str = '<table border = "1" ><tr><th>Item</th><th>Price</th><th>Quantity</th><th>Method</th><th>Total</th></tr>';
    foreach($payments as $pay){
        $str.= '<tr><td>'.$pay->getItemsName().'</td><td>'.$pay->getPrice().'</td><td>'.$pay->getQuantity().'</td><td>'.$pay->paymentMethod().'</td><td>'.$pay->getTotalPrice().'</td></tr>';
    }
    $str .= '</table>';

    return $str;
}
echo '<p  style="font-size:25px">3.Display all sales ordering by biggest total amount</p>';
usort($payments, fn($s1, $s2) => $s1->getTotalPrice() <=> $s2->getTotalPrice());
echo displayTableResult($payments);
?>